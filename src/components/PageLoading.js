import React from "react";

import "./styles/PageLoading.css";

import Loader from "../images/loader.svg";

function PageLoading() {
  return (
    <div className="PageLoading">
      <img className="PageLoading-logo" src={Loader} alt="Conf logo" />
      <p>Cargando...</p>
    </div>
  );
}

export default PageLoading;
