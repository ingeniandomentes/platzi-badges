import React from "react";

import "./styles/PageError.css";

import Error from "../images/error.svg";

function PageError(props) {
  return (
    <div className="PageError">
      <img className="PageError-logo" src={Error} alt="Conf logo" />
      <p>{props.error.message}</p>
    </div>
  );
}

export default PageError;
