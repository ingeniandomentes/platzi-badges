import React, { Component } from "react";

class BadgeForm extends Component {
  //state = {};

  //   handleChange = (e) => {
  //     // console.log({
  //     //   name: e.target.name,
  //     //   value: e.target.value,
  //     // });

  //     this.setState({
  //       [e.target.name]: e.target.value,
  //     });
  //   };

  handleClick = () => {
    console.log("Button was clicked");
  };

  // handleSubmit = (e) => {
  //   e.preventDefault();
  //   console.log("Form was submitted");
  // };

  render() {
    return (
      <div>
        {this.props.error ? (
          <p>{this.props.error.message}</p>
        ) : (
          <form onSubmit={this.props.onSubmit}>
            <div className="form-group mt-2">
              <label>First Name</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                type="text"
                name="firstName"
                value={this.props.formValues.firstName}
              />
            </div>
            <div className="form-group mt-2">
              <label>Last Name</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                type="text"
                name="lastName"
                value={this.props.formValues.lastName}
              />
            </div>
            <div className="form-group mt-2">
              <label>Email</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                type="email"
                name="email"
                value={this.props.formValues.email}
              />
            </div>
            <div className="form-group mt-2">
              <label>Job Title</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                type="text"
                name="jobTitle"
                value={this.props.formValues.jobTitle}
              />
            </div>
            <div className="form-group mt-2">
              <label>Twitter</label>
              <input
                onChange={this.props.onChange}
                className="form-control"
                type="text"
                name="twitter"
                value={this.props.formValues.twitter}
              />
            </div>
            <button onClick={this.handleClick} className="btn btn-primary mt-2">
              Save
            </button>
          </form>
        )}
      </div>
    );
  }
}

export default BadgeForm;
