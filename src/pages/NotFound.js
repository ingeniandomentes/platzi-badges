import React from "react";
import { Link } from "react-router-dom";

import "./styles/Home.css";
import astronautsImage from "../images/404.svg";

const NotFound = () => {
  return (
    <div className="Home">
      <div className="container">
        <div className="row">
          <div className="Home__col col-12 col-md-4">
            <h1>404: Page not found</h1>
            <Link className="btn btn-primary" to="/">
              Back Home
            </Link>
          </div>

          <div className="Home__col d-none d-md-block col-md-8">
            <img
              src={astronautsImage}
              alt="Astronauts"
              className="img-fluid p-4"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
