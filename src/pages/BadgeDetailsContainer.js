import React, { Component } from "react";

import PageLoading from "../components/PageLoading";
import PageError from "../components/PageError";

import api from "../api";
import BadgeDetails from "./BadgeDetails";

class BadgeDetailsContainer extends Component {
  state = {
    loading: true,
    error: null,
    data: undefined,
    modalIsOpen: false,
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    this.setState({ ...this.state, loading: true, error: null });
    try {
      const data = await api.badges.read(this.props.match.params.badgeId);
      this.setState({ ...this.state, loading: false, data: data });
    } catch (error) {
      this.setState({ ...this.state, loading: false, error: error });
    }
  };

  handleCloseModal = (event) => {
    this.setState({ ...this.state, modalIsOpen: !this.state.modalIsOpen });
  };

  handleDeleteBadge = async () => {
    this.setState({ ...this.state, loading: true, error: null });
    try {
      await api.badges.remove(this.props.match.params.badgeId);
      this.setState({ ...this.state, loading: false });
      this.props.history.push("/badges");
    } catch (error) {
      this.setState({ ...this.state, loading: false, error: error });
    }
  };

  render() {
    if (this.state.loading) {
      return <PageLoading />;
    }

    if (this.state.error) {
      return <PageError error={this.state.error} />;
    }

    return (
      <BadgeDetails
        badge={this.state.data}
        onCloseModal={this.handleCloseModal}
        onDeleteBadge={this.handleDeleteBadge}
        modalIsOpen={this.state.modalIsOpen}
      />
    );
  }
}

export default BadgeDetailsContainer;
